# sbt-bom uberjar creation

This project is a fork https://github.com/siculo/sbt-bom meant for the creation of an uberjar for use in offline environments.

## How to create the uberjar

Above commands in an `sbt` docker container:

```bash
git clone -b v0.3.0 https://github.com/siculo/sbt-bom sbt-bom-src
cat >> ./sbt-bom-src/build.sbt <<EOL
assembly / assemblyMergeStrategy := {
  case PathList("META-INF", _*) => MergeStrategy.last
  case _ => MergeStrategy.first
}
EOL
echo 'addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "2.1.1")' >> ./sbt-bom-src/project/plugins.sbt
docker run -it --rm -v ./sbt-bom-src/$(pwd):/src -w /src hseeberger/scala-sbt:8u312_1.6.2_3.1.1 sbt clean assembly
```

An uberjar will be created in the `target` folder, its location logged to standard out (e.g. `[info] Built: /src/target/scala-2.12/sbt-1.0/sbt-bom-assembly-0.3.0.jar`).

## How to use the uberjar

### Example with gemnasium-maven and scala-sbt

```bash
git clone https://gitlab.com/gitlab-org/security-products/tests/scala-sbt scala-sbt
cp ./sbt-bom-src/target/scala-2.12/sbt-1.0/sbt-bom-assembly-0.3.0.jar scala-sbt
docker run -it --entrypoint '' -w /src -v $(pwd)/scala-sbt:/src gemnasium-maven bash
echo 'addSbtPlugin("io.github.siculo" % "sbt-bom" % "0.3.0" from "file:///src/sbt-bom-assembly-0.3.0.jar")' > /root/.sbt/1.0/plugins/plugins.sbt
sbt clean makeBom
```

An `xml` file will be written (e.g. `[info] Bom file /src/target/hello-0.1.0-SNAPSHOT.bom.xml created`).
